# GAME OF DRONES
Game of Drones Project by Iván Ponce (ivanponcedev@gmail.com). Code Challenge for UruIT.

### Running the project

In order to run up GameOfDrones project in local environment, is important consider:
 - You need have Docker installed
 - You need have a complete .env_server file located in *game_of_drones/* with the following fields:

 ```
    SETTINGS_MODULE=game_of_drones.game_of_drones.settings.dev
    SECRET_KEY=bdu*i*(@p8n9(4zk-u@@-g4kvi*p+_19sr=6awl0%d(-9-4-e)
 ```

Once the environment variables are completed, run the project is so simple as to be on the root of the project and execute: `docker-compose up`

GraphQL is used as communicate way with future front-end project. For this purpose many schemas (queries) and mutations were created.
In order to probe these queries and mutations you can use a QraphQL client on localhost:8000 (if you are running local the project),
or http://167.99.227.113:8000/ (host of project on web).

### Querying Players
For search existing players in the system you can execute the following query:

```
query{
  players{
    edges{
      node{
        id
        name
      }
    }
  }
}
```

### Querying Matches
For search existing matches in the system you can execute the following query:

```
query{
  matches{
    edges{
      node{
        id
        firstPlayer{
          name
        }
        secondPlayer{
          name
        }
        winnerPlayer{
          name
        }
        rounds{
          edges{
            node{
              firstPlayerMove
              secondPlayerMove
              winnerPlayer
            }
          }
        }
      }
    }
  }
}
```

### Creating a Player
For create a new player  in the system you can execute the following mutation:

```
mutation{
  createPlayer(
  	name:"Julian"
  )
  {
    ok
    message
    player{
      id
      name
    }
  }
}
```

### Creating a Match
For create a new match in the system you can execute the following mutation:

```
mutation{
  createMatch(
    firstPlayerName: "Alex",
    secondPlayerName: "Julian"
  )
  {
    ok
    message
    match{
      id
      firstPlayer{
        name
      }
      secondPlayer{
        name
      }
    }
  }
}
```

### Ask if it's possible get another round for a match
For ask if it's possible get another round for a match you can execute the following mutation (to identify match please use id given by macth query):

```
mutation{
  canGetOtherRound(
    matchId:"TWF0Y2hOb2RlOjM="
  ){
    ok
    message
    result
  }
}
```

### Play a new round
In order to play another round for a match you can execute the following mutation (for moves, the accepted values are: STONE, PAPER, SCISSORS):
```
mutation{
  newRound(
    matchId:"TWF0Y2hOb2RlOjM=",
    firstPlayerMove:STONE,
    secondPlayerMove:PAPER
  ){
    ok
    message
    round {
      id
    }
  }
}
```
