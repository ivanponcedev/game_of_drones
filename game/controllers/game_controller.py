import logging
from game.models import Round

logger = logging.getLogger('game')


class GameController:

    def __init__(self, match):
        assert match
        self._match = match

    def can_get_other_round(self):
        """
        Is possible get another round for the match?.
        :return: flag it's possible get another round for this match
        """
        rounds = self._match.rounds
        other_round = True
        message = 'Time for another Round!'

        if rounds.filter(winner_player=1).count() == 3:
            self._match.winner_player = self._match.first_player
            self._match.save()
            message = f"The game ends. {self._match.first_player} is the new emperor!"
            other_round = False
        elif rounds.filter(winner_player=2).count() == 3:
            self._match.winner_player = self._match.second_player
            self._match.save()
            message = f"The game ends. {self._match.second} is the new emperor!"
            other_round = False

        logger.debug(f"GameController.can_get_other_round, result: {other_round} | {message}", extra={
            'match': self._match})

        return other_round, message

    def new_round(self, first_player_move, second_player_move):
        """
        Create a new round for the match.
        :param first_player_move: move for the first player
        :param second_player_move: move for the second player
        :return: creation result (True or False), message, Round object
        """
        try:
            game_round = Round.objects.create(
                match=self._match,
                first_player_move=first_player_move,
                second_player_move=second_player_move
            )
            game_round.calculate_result()
            logger.debug(f"GameController.new_round: player1:{first_player_move} vs. player2:{second_player_move}",
                         extra={'match': self._match})
            return True, f'Round won by player {game_round.winner_player}', game_round

        except Exception as e:
            logger.debug(f"GameController.new_round: error in creation of new round : {e}",
                         extra={'match': self._match})
            print(e)
            return False, 'Round creation failed, please check application logs', None
