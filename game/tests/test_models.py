from django.test import TestCase
from .factories import PlayerFactory, MatchFactory
from game.models import Match
from game.controllers.game_controller import GameController


class GameModelsTestCase(TestCase):

    def setUp(self):
        player_1 = PlayerFactory.create(name='John')
        player_2 = PlayerFactory.create(name='Alex')
        MatchFactory.create(first_player=player_1, second_player=player_2)

    def test_calculate_points_in_match(self):
        match = Match.objects.last()
        controller = GameController(match)

        controller.new_round('stone', 'scissors')
        controller.new_round('stone', 'paper')
        assert match.calculate_points() == (1, 1)

        controller.new_round('stone', 'paper')
        assert match.calculate_points() == (1, 2)

    def test_calculate_result_in_round(self):
        match = Match.objects.last()
        controller = GameController(match)

        ok, message, round = controller.new_round('stone', 'scissors')

        assert round.winner_player == 1
