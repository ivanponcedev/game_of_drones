from django.test import TestCase
from .factories import PlayerFactory, MatchFactory, RoundFactory


class GameCreationTestCase(TestCase):

    def test_create_player(self):
        player = PlayerFactory.create(name='John')
        assert player.name == 'John'

    def test_create_match(self):
        player_1 = PlayerFactory.create(name='John')
        player_2 = PlayerFactory.create(name='Alex')
        match = MatchFactory.create(first_player=player_1, second_player=player_2)
        assert match.first_player.name == 'John'
        assert match.second_player.name == 'Alex'
        assert str(match) == 'John Vs. Alex'

    def test_creating_round(self):
        player_1 = PlayerFactory.create(name='John')
        player_2 = PlayerFactory.create(name='Alex')
        match = MatchFactory.create(first_player=player_1, second_player=player_2)
        round = RoundFactory.create(match=match, first_player_move='paper', second_player_move='paper')
        assert str(round) == 'Match: John Vs. Alex | paper - paper'
