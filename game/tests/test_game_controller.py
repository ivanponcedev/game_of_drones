from django.test import TestCase
from .factories import PlayerFactory, MatchFactory
from game.models import Match
from game.controllers.game_controller import GameController


class GameControllerTestCase(TestCase):

    def setUp(self):
        player_1 = PlayerFactory.create(name='John')
        player_2 = PlayerFactory.create(name='Alex')
        MatchFactory.create(first_player=player_1, second_player=player_2)

    def test_players_can_select_their_move(self):
        match = Match.objects.last()
        controller = GameController(match)

        ok, message, round = controller.new_round('stone', 'scissors')
        assert match.rounds.count() == 1
        assert ok is True
        assert message == 'Round won by player 1'
        assert round.winner_player == 1

        ok, message, round = controller.new_round('stone', 'paper')
        assert match.rounds.count() == 2
        assert ok is True
        assert message == 'Round won by player 2'
        assert round.winner_player == 2

    def test_can_get_another_round(self):
        match = Match.objects.last()
        controller = GameController(match)

        controller.new_round('stone', 'scissors')
        controller.new_round('stone', 'scissors')
        controller.new_round('stone', 'paper')
        controller.new_round('stone', 'paper')

        assert match.rounds.count() == 4
        assert controller.can_get_other_round() == (True, 'Time for another Round!')

        controller.new_round('stone', 'scissors')

        assert match.rounds.count() == 5
        assert controller.can_get_other_round() == (False, 'The game ends. John is the new emperor!')
