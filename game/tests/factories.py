import factory

from game.models import Player, Round, Match


class PlayerFactory(factory.django.DjangoModelFactory):
    class Meta:
        django_get_or_create = ('name',)
        model = Player


class RoundFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Round


class MatchFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Match
