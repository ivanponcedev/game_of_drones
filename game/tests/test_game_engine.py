from django.test import TestCase
from game.game_engine import FigureFactory


class GameEngineTestCase(TestCase):

    def test_create_figures(self):
        stone = FigureFactory.get_figure('stone')
        paper = FigureFactory.get_figure('paper')
        scissors = FigureFactory.get_figure('scissors')

        assert stone.__class__.__name__ == 'Stone'
        assert paper.__class__.__name__ == 'Paper'
        assert scissors.__class__.__name__ == 'Scissors'

    def test_stone_behavior(self):
        stone = FigureFactory.get_figure('stone')
        paper = FigureFactory.get_figure('paper')
        scissors = FigureFactory.get_figure('scissors')

        assert stone.fight_with_other_figure(stone) == 0
        assert stone.fight_with_other_figure(paper) == 2
        assert stone.fight_with_other_figure(scissors) == 1

    def test_paper_behavior(self):
        stone = FigureFactory.get_figure('stone')
        paper = FigureFactory.get_figure('paper')
        scissors = FigureFactory.get_figure('scissors')

        assert paper.fight_with_other_figure(stone) == 1
        assert paper.fight_with_other_figure(paper) == 0
        assert paper.fight_with_other_figure(scissors) == 2

    def test_scissors_behavior(self):
        stone = FigureFactory.get_figure('stone')
        paper = FigureFactory.get_figure('paper')
        scissors = FigureFactory.get_figure('scissors')

        assert scissors.fight_with_other_figure(stone) == 2
        assert scissors.fight_with_other_figure(paper) == 1
        assert scissors.fight_with_other_figure(scissors) == 0
