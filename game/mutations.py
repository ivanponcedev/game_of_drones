import base64
import graphene
from .schemas import PlayerNode, MatchNode, RoundNode
from .models import Player, Match
from game.controllers.game_controller import GameController


class CreatePlayerMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)

    player = graphene.Field(PlayerNode)
    ok = graphene.Boolean()
    message = graphene.String()

    def mutate(self, info, **args):
        try:
            name = args.get('name')
            player = Player.objects.get_or_create(name=name)[0]

            ok = True
            message = 'Successfully obtained player'

        except Exception as e:
            ok = False
            message = str(e)
            player = None

        response = CreatePlayerMutation(ok=ok, message=message, player=player)
        return response


class CreateMatch(graphene.Mutation):

    class Arguments:
        first_player_name = graphene.String(required=True)
        second_player_name = graphene.String(required=True)

    match = graphene.Field(MatchNode)
    ok = graphene.Boolean()
    message = graphene.String()

    def mutate(self, info, **args):
        try:
            first_player_name = args.get('first_player_name')
            second_player_name = args.get('second_player_name')

            first_player = Player.objects.get(name=first_player_name)
            second_player = Player.objects.get(name=second_player_name)
            match = Match.objects.create(first_player=first_player, second_player=second_player)

            ok = True
            message = 'Successfully created match'

        except Exception as e:
            ok = False
            message = str(e)
            match = None

        response = CreateMatch(ok=ok, message=message, match=match)
        return response


class CanGetOtherRound(graphene.Mutation):

    class Arguments:
        match_id = graphene.String(required=True)

    result = graphene.Boolean()
    ok = graphene.Boolean()
    message = graphene.String()

    def mutate(self, info, **args):
        try:

            coded_string = args.get('match_id')
            decoded = base64.b64decode(coded_string).decode("utf-8")
            match_id = int(decoded.split(':')[1])
            match = Match.objects.get(pk=match_id)

            contoller = GameController(match)
            result, message = contoller.can_get_other_round()
            ok = True

        except Exception as e:
            ok = False
            message = str(e)
            result = None

        response = CanGetOtherRound(ok=ok, message=message, result=result)
        return response


class MovesOptions(graphene.Enum):
    STONE = 'stone'
    PAPER = 'paper'
    SCISSORS = 'scissors'


class NewRound(graphene.Mutation):

    class Arguments:
        match_id = graphene.String(required=True)
        first_player_move = MovesOptions(required=True)
        second_player_move = MovesOptions(required=True)

    message = graphene.String()
    ok = graphene.Boolean()
    round = graphene.Field(RoundNode)

    def mutate(self, info, **args):
        try:

            coded_string = args.get('match_id')
            decoded = base64.b64decode(coded_string).decode("utf-8")
            match_id = int(decoded.split(':')[1])
            match = Match.objects.get(pk=match_id)

            contoller = GameController(match)
            ok, message, round = contoller.new_round(args.get('first_player_move'), args.get('second_player_move'))

        except Exception as e:
            ok = False
            message = str(e)
            round = None

        response = NewRound(ok=ok, message=message, round=round)
        return response
