from graphene import relay
from graphene_django import DjangoObjectType
from .models import Player, Match, Round


class PlayerNode(DjangoObjectType):

    class Meta:
        model = Player
        interfaces = (relay.Node,)
        filter_fields = []


class MatchNode(DjangoObjectType):

    class Meta:
        model = Match
        interfaces = (relay.Node,)
        filter_fields = []


class RoundNode(DjangoObjectType):

    class Meta:
        model = Round
        interfaces = (relay.Node,)
        filter_fields = []
