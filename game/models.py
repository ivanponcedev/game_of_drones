import logging
from django.db import models
from .choices import MOVE_OPTIONS
from .game_engine import FigureFactory

logger = logging.getLogger('game')


class Player(models.Model):
    """
    Game Player.
    """
    name = models.CharField(
        max_length=255,
        unique=True
    )

    def __str__(self):
        return self.name


class Match(models.Model):
    """
    Game Match.
    """
    first_player = models.ForeignKey(
        Player,
        related_name='matches_as_frist_player'
    )

    second_player = models.ForeignKey(
        Player,
        related_name='matches_as_second_player'
    )

    winner_player = models.ForeignKey(
        Player,
        null=True,
        blank=True,
        related_name='matches_won'
    )

    created = models.DateTimeField(auto_now_add=True)

    def calculate_points(self):
        """
        calculate the current points
        :return: tuple with count of point, (player1_points, player2_points)
        """
        rounds = self.rounds
        result = rounds.filter(winner_player=1).count(), rounds.filter(winner_player=2).count()
        logger.debug(f"Match.calculate_points: {result}", extra={'match': self.id})
        return result

    def __str__(self):
        return f'{self.first_player} Vs. {self.second_player}'


class Round(models.Model):
    """
    Round of the match.
    """
    match = models.ForeignKey(
        Match,
        related_name='rounds'
    )

    first_player_move = models.CharField(
        max_length=20,
        choices=MOVE_OPTIONS,
        default='activa'
    )

    second_player_move = models.CharField(
        max_length=20,
        choices=MOVE_OPTIONS,
        default='activa'
    )

    winner_player = models.IntegerField(
        null=True,
        blank=True,
        help_text='number of the player who wins, 0 for tie'
    )

    def calculate_result(self):
        try:
            first_move = FigureFactory.get_figure(self.first_player_move)
            second_move = FigureFactory.get_figure(self.second_player_move)
            self.winner_player = first_move.fight_with_other_figure(second_move)
            self.save()
            logger.debug(f"Round.calculate_result: winner_player: {self.winner_player}", extra={'match': self.match_id})
        except Exception as e:
            logger.debug(f"Round.calculate_result: error: {e}", extra={'match': self.match_id})

    def __str__(self):
        return f'Match: {self.match} | {self.first_player_move} - {self.second_player_move}'
