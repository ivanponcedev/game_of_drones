from abc import ABCMeta, abstractmethod


class Figure(metaclass=ABCMeta):

    def __init__(self):
        self._functions = {
            'Stone': self._fight_with_stone,
            'Paper': self._fight_with_paper,
            'Scissors': self._fight_with_scissors
        }

    def fight_with_other_figure(self, figure):
        """
        If the return is 1, the winner is the current figure
        If the return is 2, the winner is the defient figure (get by parameter)
        If the return is 0, the result is a tie
        """
        function = self._functions.get(figure.__class__.__name__)
        return function()

    @abstractmethod
    def _fight_with_stone(self):
        pass

    @abstractmethod
    def _fight_with_paper(self):
        pass

    @abstractmethod
    def _fight_with_scissors(self):
        pass


class Stone(Figure):
    """
    Abstraction of the Stone behavior in a match
    """

    def _fight_with_stone(self):
        return 0

    def _fight_with_paper(self):
        return 2

    def _fight_with_scissors(self):
        return 1


class Paper(Figure):
    """
    Abstraction of the Paper behavior in a match
    """

    def _fight_with_stone(self):
        return 1

    def _fight_with_paper(self):
        return 0

    def _fight_with_scissors(self):
        return 2


class Scissors(Figure):
    """
    Abstraction of the Scissors behavior in a match
    """

    def _fight_with_stone(self):
        return 2

    def _fight_with_paper(self):
        return 1

    def _fight_with_scissors(self):
        return 0


class FigureFactory:
    """
    Factory for Figures objects
    """

    _classes = {
        'stone': Stone,
        'paper': Paper,
        'scissors': Scissors,
    }

    @classmethod
    def get_figure(cls, figure) -> Figure:
        """
        Get a Figure object by its name
        :param figure: string with the name of the figure
        :return: Figure object
        """
        return cls._classes.get(figure)()
