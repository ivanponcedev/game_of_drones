from fabric.operations import run
from fabric.state import env
from fabric.context_managers import cd
from fabric.api import warn_only


environments = {
    'production': {
        'hosts': 'root@167.99.227.113',
        'home': '/var/game_of_drones/game_of_drones',
        'docker_commands': [
            # 'docker rm -f $(docker ps -aq)',
            # 'docker rmi -f $(docker images -aq)',
            # 'docker volume rm $(docker volume ls -qf dangling=true)',
            'docker-compose up -d',
        ],
        'git': {
            'parent': 'origin',
            'branch': 'master',
        },
    },

}


# setup
def production():
    environments['default'] = environments['production']
    env.hosts = environments['production']['hosts']


def git_pull():
    with cd(environments['default']['home']):
        run('git reset --hard')
        run('git pull %s %s' % (environments['default']['git']['parent'],
                                environments['default']['git']['branch']))


def docker_commands():
    with warn_only():
        with cd(environments['default']['home']):
            for command in environments['default']['docker_commands']:
                run(command)


def deploy():
    git_pull()
    docker_commands()
