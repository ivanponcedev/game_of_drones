from graphene import ObjectType, Schema, relay
from graphene_django.filter import DjangoFilterConnectionField
from game.schemas import PlayerNode, MatchNode, RoundNode
from game.mutations import CreatePlayerMutation, CreateMatch, CanGetOtherRound, NewRound


class GlobalQuery(ObjectType):
    """
    GlobalQuery
    """
    match = relay.Node.Field(MatchNode)
    matches = DjangoFilterConnectionField(MatchNode)

    player = relay.Node.Field(PlayerNode)
    players = DjangoFilterConnectionField(PlayerNode)

    round = relay.Node.Field(RoundNode)
    rounds = DjangoFilterConnectionField(RoundNode)


class MutationRoot(ObjectType):
    """
    MutationRoot
    """
    create_player = CreatePlayerMutation.Field()
    create_match = CreateMatch.Field()
    can_get_other_round = CanGetOtherRound.Field()
    new_round = NewRound.Field()


global_schema = Schema(query=GlobalQuery, mutation=MutationRoot)
