from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from api.schemas import global_schema


app_name = 'api'

urlpatterns = [
    url(r'^$', csrf_exempt(GraphQLView.as_view(schema=global_schema, graphiql=True))),
]
